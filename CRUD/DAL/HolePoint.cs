﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using JetBrains.Annotations;

namespace CRUD.DAL;

[UsedImplicitly]
public class HolePoint
{
    public int Id { get; set; }

    public int X { get; set; }
    public int Y { get; set; }
    public int Z { get; set; }

    [JsonIgnore] public int HoleId { get; set; }

    // https://learn.microsoft.com/ru-ru/ef/core/miscellaneous/nullable-reference-types#non-nullable-properties-and-initialization
    [ForeignKey("HoleId")] [JsonIgnore] public virtual Hole Hole { get; set; } = null!;
}