﻿using JetBrains.Annotations;

namespace CRUD.DAL;

[UsedImplicitly]
public class DrillBlock
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public DateTime UpdateDate { get; set; }

    public virtual ICollection<Hole> Holes { get; set; } = new List<Hole>();
    public virtual ICollection<DrillBlockPoint> Points { get; set; } = new List<DrillBlockPoint>();
}