﻿using System.Text.Json.Serialization;
using JetBrains.Annotations;

namespace CRUD.DAL;

[UsedImplicitly]
public class DrillBlockPoint
{
    public int Id { get; set; }

    public int X { get; set; }
    public int Y { get; set; }
    public int Z { get; set; }

    [JsonIgnore] public int DrillBlockId { get; set; }

    // https://learn.microsoft.com/ru-ru/ef/core/miscellaneous/nullable-reference-types#non-nullable-properties-and-initialization
    [JsonIgnore] public virtual DrillBlock DrillBlock { get; set; } = null!;
}