﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;
using JetBrains.Annotations;

namespace CRUD.DAL;

[UsedImplicitly]
public class Hole
{
    public int Id { get; set; }
    public string? Name { get; set; }
    public float Depth { get; set; }

    [JsonIgnore] public int DrillBlockId { get; set; }

    [ForeignKey("DrillBlockId")]
    [JsonIgnore]
    public virtual DrillBlock DrillBlock { get; set; } = null!;

    public virtual HolePoint? Point { get; set; }
}