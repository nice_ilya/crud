﻿using Microsoft.EntityFrameworkCore;

namespace CRUD.DAL;

public sealed class Context : DbContext
{
    public DbSet<DrillBlock> DrillBlocks { get; set; }
    public DbSet<DrillBlockPoint> DrillBlocksPoints { get; set; }

    public DbSet<Hole> Holes { get; set; }
    public DbSet<HolePoint> HolesPoints { get; set; }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    public Context(DbContextOptions<Context> options) : base(options)
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    {
    }
}