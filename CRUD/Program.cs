using System.Text.Json.Serialization;
using CRUD.DAL;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

ConfigureDatabaseContext(builder.Services, builder.Configuration.GetConnectionString("defaultConnectionString"));
ConfigureServices(builder.Services);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(options => { options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1"); });

    app.UseHealthChecks("/health");
}

// app.UseHttpsRedirection();

// app.UseAuthorization();

app.MapControllers();
app.UseDeveloperExceptionPage();
app.Run();
return;

void ConfigureServices(IServiceCollection services)
{
    services.AddControllers().AddJsonOptions(options =>
    {
        options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
    });

    services.AddHealthChecks();

    services.AddEndpointsApiExplorer();
    services.AddSwaggerGen();
}

void ConfigureDatabaseContext(IServiceCollection services, string? connectionString)
{
    if (connectionString is null) throw new ArgumentNullException(nameof(connectionString));

    services.AddDbContext<Context>(
        options => { options.UseNpgsql(connectionString); });
}