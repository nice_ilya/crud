﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CRUD.Migrations
{
    /// <inheritdoc />
    public partial class RenameHolePointsCollection : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HolePoints_Holes_HoleId",
                table: "HolePoints");

            migrationBuilder.DropPrimaryKey(
                name: "PK_HolePoints",
                table: "HolePoints");

            migrationBuilder.RenameTable(
                name: "HolePoints",
                newName: "HolesPoints");

            migrationBuilder.RenameIndex(
                name: "IX_HolePoints_HoleId",
                table: "HolesPoints",
                newName: "IX_HolesPoints_HoleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_HolesPoints",
                table: "HolesPoints",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_HolesPoints_Holes_HoleId",
                table: "HolesPoints",
                column: "HoleId",
                principalTable: "Holes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HolesPoints_Holes_HoleId",
                table: "HolesPoints");

            migrationBuilder.DropPrimaryKey(
                name: "PK_HolesPoints",
                table: "HolesPoints");

            migrationBuilder.RenameTable(
                name: "HolesPoints",
                newName: "HolePoints");

            migrationBuilder.RenameIndex(
                name: "IX_HolesPoints_HoleId",
                table: "HolePoints",
                newName: "IX_HolePoints_HoleId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_HolePoints",
                table: "HolePoints",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_HolePoints_Holes_HoleId",
                table: "HolePoints",
                column: "HoleId",
                principalTable: "Holes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
