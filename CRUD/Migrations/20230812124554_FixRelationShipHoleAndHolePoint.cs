﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CRUD.Migrations
{
    /// <inheritdoc />
    public partial class FixRelationShipHoleAndHolePoint : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Holes_HolePoints_HolePointId",
                table: "Holes");

            migrationBuilder.DropIndex(
                name: "IX_Holes_HolePointId",
                table: "Holes");

            migrationBuilder.DropIndex(
                name: "IX_HolePoints_HoleId",
                table: "HolePoints");

            migrationBuilder.DropColumn(
                name: "HolePointId",
                table: "Holes");

            migrationBuilder.CreateIndex(
                name: "IX_HolePoints_HoleId",
                table: "HolePoints",
                column: "HoleId",
                unique: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_HolePoints_HoleId",
                table: "HolePoints");

            migrationBuilder.AddColumn<int>(
                name: "HolePointId",
                table: "Holes",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Holes_HolePointId",
                table: "Holes",
                column: "HolePointId");

            migrationBuilder.CreateIndex(
                name: "IX_HolePoints_HoleId",
                table: "HolePoints",
                column: "HoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_Holes_HolePoints_HolePointId",
                table: "Holes",
                column: "HolePointId",
                principalTable: "HolePoints",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
