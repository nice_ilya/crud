﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CRUD.Migrations
{
    /// <inheritdoc />
    public partial class FixRelationshipsDAL : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Holes_HolePointId",
                table: "Holes");

            migrationBuilder.CreateIndex(
                name: "IX_Holes_HolePointId",
                table: "Holes",
                column: "HolePointId");

            migrationBuilder.CreateIndex(
                name: "IX_HolePoints_HoleId",
                table: "HolePoints",
                column: "HoleId");

            migrationBuilder.AddForeignKey(
                name: "FK_HolePoints_Holes_HoleId",
                table: "HolePoints",
                column: "HoleId",
                principalTable: "Holes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_HolePoints_Holes_HoleId",
                table: "HolePoints");

            migrationBuilder.DropIndex(
                name: "IX_Holes_HolePointId",
                table: "Holes");

            migrationBuilder.DropIndex(
                name: "IX_HolePoints_HoleId",
                table: "HolePoints");

            migrationBuilder.CreateIndex(
                name: "IX_Holes_HolePointId",
                table: "Holes",
                column: "HolePointId",
                unique: true);
        }
    }
}
