﻿using System.Net.Mime;
using CRUD.DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace CRUD.Controllers;

[ApiController]
[Route("api/[controller]")]
public class DrillBlocksController : ControllerBase
{
    private readonly ILogger<DrillBlocksController> _logger;

    private readonly Context _context;
    private readonly DbSet<DrillBlock> _drillBlocks;

    public DrillBlocksController(Context context, ILogger<DrillBlocksController> logger)
    {
        _logger = logger;
        _context = context;
        _drillBlocks = _context.DrillBlocks;
    }

    #region Create

    [HttpPost(Name = "CreateDrillBlock")]
    [Consumes(MediaTypeNames.Application.Json)]
    [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(DrillBlock))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> CreateAsync(DrillBlock drillBlock)
    {
        try
        {
            var newDrillBlockResult = await _drillBlocks.AddAsync(drillBlock);
            await _context.SaveChangesAsync();

            return CreatedAtRoute("GetDrillBlockById", new {id = newDrillBlockResult.Entity.Id}, drillBlock);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "");
            return Problem("Unable to Create the drill block");
        }
    }

    #endregion

    #region Read

    private async Task<DrillBlock?> GetById(int id)
    {
        return await _drillBlocks.FindAsync(id);
    }

    private bool DrillBlockExists(int id)
    {
        return _drillBlocks.Any(drillBlock => drillBlock.Id == id);
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<DrillBlock>))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<IEnumerable<DrillBlock>>> GetAll()
    {
        var result = await _drillBlocks
            .Include(drillBlock => drillBlock.Points)
            .Include(drillBlock => drillBlock.Holes)
            .ThenInclude(hole => hole.Point)
            .ToListAsync();

        return result;
    }

    [HttpGet("{id:int}", Name = "GetDrillBlockById")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(DrillBlock))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> GetAsync(int id)
    {
        try
        {
            var drillBlock = await GetById(id);
            if (drillBlock is null) return NotFound();

            return Ok(drillBlock);
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "");
            return Problem("Unable to Get the drill block");
        }
    }

    #endregion Read

    #region Update

    [HttpPut(Name = "UpdateDrillBlock")]
    [Consumes(MediaTypeNames.Application.Json)]
    [ProducesResponseType(StatusCodes.Status202Accepted, Type = typeof(DrillBlock))]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<IActionResult> UpdateAsync(int id, DrillBlock drillBlock)
    {
        if (id != drillBlock.Id) return BadRequest();

        _context.Entry(drillBlock).State = EntityState.Modified;

        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!DrillBlockExists(id)) return NotFound();

            throw;
        }

        return Accepted(drillBlock);
    }

    #endregion Update

    #region Delete

    [HttpDelete("{id:int}", Name = "DeleteDrillBlock")]
    [Consumes(MediaTypeNames.Application.Json)]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public async Task<IActionResult> DeleteAsync(int id)
    {
        try
        {
            var drillBlock = await GetById(id);

            if (drillBlock is null) return NotFound();

            _drillBlocks.Remove(drillBlock);
            return Ok();
        }
        catch (Exception exception)
        {
            _logger.LogError(exception, "");
            return Problem("Unable to Delete the concert");
        }
    }

    #endregion Delete
}